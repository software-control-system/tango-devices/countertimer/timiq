//=============================================================================
// TimIQManager.h
//=============================================================================
// abstraction.......TimIQManager for TimIQ
// class.............TimIQManager
// original author...S.Minolli - Nexeya
//=============================================================================

#ifndef _TIMIQ_MANAGER_H
#define _TIMIQ_MANAGER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include<timiq/TIMIQLib.h>


namespace TIMIQ_ns
{

// ============================================================================
// TimIQConfig:  struct containing the task configuration
// ============================================================================
typedef struct TimIQConfig
{
  //- members
  //- host device
  Tango::DeviceImpl * hostDevice;

  //- task periodic job period, in ms
  double pollingPeriod;

  //- Web server IP address
  std::string iPaddress;

  //- Web server TCP port number
  yat::uint16 tcpPort;

  //- min temperature value
  double minTemp;

  //- max temperature value
  double maxTemp;

  //- default constructor -----------------------
  TimIQConfig ()
  : hostDevice(NULL),
    pollingPeriod(1000.0),
    iPaddress(""),
    tcpPort(0),
    minTemp(0.0),
    maxTemp(0.0)
  {
  }

  //- destructor -----------------------
  ~TimIQConfig ()
  {
  }

  //- copy constructor ------------------
  TimIQConfig (const TimIQConfig& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const TimIQConfig & operator= (const TimIQConfig& src)
  {
    if (this == & src) 
      return *this;

    this->hostDevice = src.hostDevice;
    this->pollingPeriod = src.pollingPeriod;
    this->iPaddress = src.iPaddress;
    this->tcpPort = src.tcpPort;
    this->minTemp = src.minTemp;
    this->maxTemp = src.maxTemp;

    return *this;
  }

  //- dump -----------------------
  void dump () const
  {  
	  std::cout << "TimIQConfig::hostDevice........." 
		  << this->hostDevice
		  << std::endl;     
	  std::cout << "TimIQConfig::pollingPeriod........." 
		  << this->pollingPeriod
		  << std::endl;      
	  std::cout << "TimIQConfig::iPaddress........." 
		  << this->iPaddress
		  << std::endl;      
	  std::cout << "TimIQConfig::tcpPort........." 
		  << this->tcpPort
		  << std::endl;      
	  std::cout << "TimIQConfig::minTemp........." 
		  << this->minTemp
		  << std::endl;  
	  std::cout << "TimIQConfig::maxTemp........." 
		  << this->maxTemp
		  << std::endl;
  }
      
} XbpmConfig;


// ============================================================================
// class: TimIQManager
// ============================================================================
class TimIQManager :  public yat4tango::DeviceTask
{

public:

  //- constructor
  TimIQManager (Tango::DeviceImpl * hostDevice);

  //- destructor
  ~TimIQManager ();

  //- init
  void init(TimIQConfig& cfg)
	  throw (Tango::DevFailed);

  //- gets manager state
  Tango::DevState get_state();

  //- gets manager status
  std::string get_status();

  //- gets board temperature
  double get_boardTemperature()
    throw (Tango::DevFailed);

  //- gets I value
  double get_iValue()
    throw (Tango::DevFailed);

  //- gets Q value
  double get_qValue()
    throw (Tango::DevFailed);

  //- gets mixer COS value
  double get_mixerCosOutput()
    throw (Tango::DevFailed);

  //- gets mixer SIN value
  double get_mixerSinOutput()
    throw (Tango::DevFailed);

  //- sets new I value
  void set_iValue(double val)
    throw (Tango::DevFailed);

  //- sets new Q value
  void set_qValue(double val)
    throw (Tango::DevFailed);

  //- sends command
  //- if have to wait for end of command, set boolean to true, false otherwise
  void set_command(TIMIQLib_ns::E_timiq_cmd_t cmd, bool have_to_wait)
    throw (Tango::DevFailed);

protected:

  //- process_message (implements yat4tango::DeviceTask pure virtual method)
  virtual void process_message (yat::Message& msg)
    throw (Tango::DevFailed);


private:

  //- intialization
  void init_i();
 
  //- periodic job
  void periodic_job_i()
    throw (Tango::DevFailed);

  //- send new I value to hardware
  void set_iValue_i(double val)
    throw (Tango::DevFailed);

  //- send new Q value to hardware
  void set_qValue_i(double val)
    throw (Tango::DevFailed);

  //- send command to hardware
  void set_command_i(TIMIQLib_ns::E_timiq_cmd_t cmd)
    throw (Tango::DevFailed);

  //- Converts a TimIQ lib exception to a TANGO exception.
  //- \param te The TimIQ lib exception.
  Tango::DevFailed timiqToTangoException (const TIMIQLib_ns::Exception& te);

  //- task configuration
  TimIQConfig m_cfg;
  
  //- manager state
  Tango::DevState m_state;

  //- manager status
  std::string m_status;

  //- TimIQ instance
  TIMIQLib_ns::TIMIQLib * m_timiq;

  //- Internal IQ values
  //---------------------------
  double m_boardTemperature;
  double m_iValue;
  double m_qValue;
  double m_mixerCosOutput;
  double m_mixerSinOutput;

};

} // namespace TIMIQ_ns

#endif // _TIMIQ_MANAGER_H
