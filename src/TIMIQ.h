//=============================================================================
//
// file :        TIMIQ.h
//
// description : Include for the TIMIQ class.
//
// project :	TIMIQ
//
// $Author: pascal_verdier $
//
// $Revision: 13293 $
// $Date: 2009-04-07 12:53:56 +0200 (Tue, 07 Apr 2009) $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source$
// $Log$
// Revision 3.5  2007/10/23 14:04:30  pascal_verdier
// Spelling mistakes correction
//
// Revision 3.4  2005/03/02 14:06:15  pascal_verdier
// namespace is different than class name.
//
// Revision 3.3  2004/10/25 14:12:00  pascal_verdier
// Minor changes.
//
// Revision 3.2  2004/09/06 09:27:05  pascal_verdier
// Modified for Tango 5 compatibility.
//
//
// copyleft :    Synchrotron SOLEIL 
//               L'Orme des merisiers - Saint Aubin
//		 BP48 - 91192 Gif sur Yvette
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _TIMIQ_H
#define _TIMIQ_H

#include <tango.h>
#include <timiq/TIMIQLib.h>
#include "TimIQManager.h"

//using namespace Tango;

/**
 * @author	$Author: pascal_verdier $
 * @version	$Revision: 13293 $
 */

 //	Add your own constant definitions here.
 //-----------------------------------------------


namespace TIMIQ_ns
{

/**
 * Class Description:
 * This device controls a Tim IQ card whose function is to de-phase a sinusoidal voltage
 *	signal from 2  voltage commands:
 *	- IValue
 *	- QValue
 */

/*
 *	Device States Description:
*  Tango::INIT :     Initialization in progress...
*  Tango::ALARM :    The Board temperature is outside the limits Tmin,Tmax.
*  Tango::FAULT :    TimIQ equipment is in fault...
*  Tango::RUNNING :  TimIQ equipment, system up and running
*  Tango::MOVING :   TimIQ equipment, updates are in progress...
 */


class TIMIQ: public Tango::Device_4Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attribute member data.
 */
//@{
		Tango::DevDouble	*attr_workingFrequency_read;
		Tango::DevDouble	*attr_boardTemperature_read;
		Tango::DevDouble	*attr_iValue_read;
		Tango::DevDouble	attr_iValue_write;
		Tango::DevDouble	*attr_qValue_read;
		Tango::DevDouble	attr_qValue_write;
		Tango::DevDouble	*attr_mixerCosOutput_read;
		Tango::DevDouble	*attr_mixerSinOutput_read;
		Tango::DevDouble	*attr_mixerMaxCosOutput_read;
		Tango::DevDouble	*attr_mixerMaxSinOutput_read;
		Tango::DevDouble	*attr_mixerCosPhase_read;
		Tango::DevDouble	*attr_mixerSinPhase_read;
//@}

/**
 * @name Device properties
 * Device properties member data.
 */
//@{
/**
 *	Embeded Web server IP address. Mandatory
 */
	string	iPaddress;
/**
 *	Embeded Web server port number. Mandatory
 */
	Tango::DevShort	port;
/**
 *	Minimum temperature, in degree Celsius, for triggering an ALARM state.
 *	
 */
	Tango::DevDouble	tmin;
/**
 *	Maximum temperature, in degree Celsius, for triggering an ALARMstate
 */
	Tango::DevDouble	tmax;
/**
 *	The TimIQ card input frequency, in Hz.
 */
	Tango::DevDouble	workingFrequency;
/**
 *	The 0 degree mixer max cosinus output, in volts.
 */
	Tango::DevDouble	mixerMaxCosOutput;
/**
 *	The 90 degree mixer max sinus output, in volts.
 */
	Tango::DevDouble	mixerMaxSinOutput;
/**
 *	The 0 degree mixer cosinus phase, in degree.
 */
	Tango::DevDouble	mixerCosPhase;
/**
 *	The 90 degree mixer sinus phase, in degree.
 */
	Tango::DevDouble	mixerSinPhase;
/**
 *	Polling period in ms.
 */
	Tango::DevDouble	pollingPeriod;
//@}

/**
 *	@name Device properties
 *	Device property member data.
 */
//@{
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	TIMIQ(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	TIMIQ(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	TIMIQ(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one destructor is defined for this class */
//@{
/**
 * The object destructor.
 */	
	~TIMIQ() {delete_device();};
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method before execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name TIMIQ methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for workingFrequency acquisition result.
 */
	virtual void read_workingFrequency(Tango::Attribute &attr);
/**
 *	Extract real attribute values for boardTemperature acquisition result.
 */
	virtual void read_boardTemperature(Tango::Attribute &attr);
/**
 *	Extract real attribute values for iValue acquisition result.
 */
	virtual void read_iValue(Tango::Attribute &attr);
/**
 *	Write iValue attribute values to hardware.
 */
	virtual void write_iValue(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for qValue acquisition result.
 */
	virtual void read_qValue(Tango::Attribute &attr);
/**
 *	Write qValue attribute values to hardware.
 */
	virtual void write_qValue(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for mixerCosOutput acquisition result.
 */
	virtual void read_mixerCosOutput(Tango::Attribute &attr);
/**
 *	Extract real attribute values for mixerSinOutput acquisition result.
 */
	virtual void read_mixerSinOutput(Tango::Attribute &attr);
/**
 *	Extract real attribute values for mixerMaxCosOutput acquisition result.
 */
	virtual void read_mixerMaxCosOutput(Tango::Attribute &attr);
/**
 *	Extract real attribute values for mixerMaxSinOutput acquisition result.
 */
	virtual void read_mixerMaxSinOutput(Tango::Attribute &attr);
/**
 *	Extract real attribute values for mixerCosPhase acquisition result.
 */
	virtual void read_mixerCosPhase(Tango::Attribute &attr);
/**
 *	Extract real attribute values for mixerSinPhase acquisition result.
 */
	virtual void read_mixerSinPhase(Tango::Attribute &attr);
/**
 *	Read/Write allowed for workingFrequency attribute.
 */
	virtual bool is_workingFrequency_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for boardTemperature attribute.
 */
	virtual bool is_boardTemperature_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for iValue attribute.
 */
	virtual bool is_iValue_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for qValue attribute.
 */
	virtual bool is_qValue_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for mixerCosOutput attribute.
 */
	virtual bool is_mixerCosOutput_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for mixerSinOutput attribute.
 */
	virtual bool is_mixerSinOutput_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for mixerMaxCosOutput attribute.
 */
	virtual bool is_mixerMaxCosOutput_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for mixerMaxSinOutput attribute.
 */
	virtual bool is_mixerMaxSinOutput_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for mixerCosPhase attribute.
 */
	virtual bool is_mixerCosPhase_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for mixerSinPhase attribute.
 */
	virtual bool is_mixerSinPhase_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for AcknowledgeErrors command.
 */
	virtual bool is_AcknowledgeErrors_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for PLLcalibration command.
 */
	virtual bool is_PLLcalibration_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Reset command.
 */
	virtual bool is_Reset_allowed(const CORBA::Any &any);
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();
/**
 * This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *	@return	Status description
 *	@exception DevFailed
 */
	virtual Tango::ConstDevString	dev_status();
/**
 * Acknowledgment of all errors.
 *	@exception DevFailed
 */
	void	acknowledge_errors();
/**
 * PLL calibration of TimIQ card.
 *	@exception DevFailed
 */
	void	pllcalibration();
/**
 * Resets the TimIQ card.
 *	@exception DevFailed
 */
	void	reset();

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	

protected :	
	//	Add your own data members here
	//-----------------------------------------

	//- TimIQManager instance
	TimIQManager * m_timiqMger;
	
	//- Internal IQ values
	//---------------------------
	double m_boardTemperature;

	//- device state and status
	Tango::DevState m_curr_state;
	std::string m_curr_status;
	
	// init done flag
	bool m_init_device_done;
 
};

}	// namespace_ns

#endif	// _TIMIQ_H
