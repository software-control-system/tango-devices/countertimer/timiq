//=============================================================================
//
// file :         TIMIQClass.h
//
// description :  Include for the TIMIQClass root class.
//                This class is the singleton class for
//                the TIMIQ device class.
//                It contains all properties and methods which the 
//                TIMIQ requires only once e.g. the commands.
//			
// project :      TANGO Device Server
//
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source:  $
// $Log:  $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _TIMIQCLASS_H
#define _TIMIQCLASS_H

#include <tango.h>
#include <TIMIQ.h>


namespace TIMIQ_ns
{//=====================================
//	Define classes for attributes
//=====================================
class mixerSinPhaseAttrib: public Tango::Attr
{
public:
	mixerSinPhaseAttrib():Attr("mixerSinPhase", Tango::DEV_DOUBLE, Tango::READ) {};
	~mixerSinPhaseAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TIMIQ *>(dev))->read_mixerSinPhase(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TIMIQ *>(dev))->is_mixerSinPhase_allowed(ty);}
};

class mixerCosPhaseAttrib: public Tango::Attr
{
public:
	mixerCosPhaseAttrib():Attr("mixerCosPhase", Tango::DEV_DOUBLE, Tango::READ) {};
	~mixerCosPhaseAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TIMIQ *>(dev))->read_mixerCosPhase(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TIMIQ *>(dev))->is_mixerCosPhase_allowed(ty);}
};

class mixerMaxSinOutputAttrib: public Tango::Attr
{
public:
	mixerMaxSinOutputAttrib():Attr("mixerMaxSinOutput", Tango::DEV_DOUBLE, Tango::READ) {};
	~mixerMaxSinOutputAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TIMIQ *>(dev))->read_mixerMaxSinOutput(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TIMIQ *>(dev))->is_mixerMaxSinOutput_allowed(ty);}
};

class mixerMaxCosOutputAttrib: public Tango::Attr
{
public:
	mixerMaxCosOutputAttrib():Attr("mixerMaxCosOutput", Tango::DEV_DOUBLE, Tango::READ) {};
	~mixerMaxCosOutputAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TIMIQ *>(dev))->read_mixerMaxCosOutput(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TIMIQ *>(dev))->is_mixerMaxCosOutput_allowed(ty);}
};

class mixerSinOutputAttrib: public Tango::Attr
{
public:
	mixerSinOutputAttrib():Attr("mixerSinOutput", Tango::DEV_DOUBLE, Tango::READ) {};
	~mixerSinOutputAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TIMIQ *>(dev))->read_mixerSinOutput(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TIMIQ *>(dev))->is_mixerSinOutput_allowed(ty);}
};

class mixerCosOutputAttrib: public Tango::Attr
{
public:
	mixerCosOutputAttrib():Attr("mixerCosOutput", Tango::DEV_DOUBLE, Tango::READ) {};
	~mixerCosOutputAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TIMIQ *>(dev))->read_mixerCosOutput(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TIMIQ *>(dev))->is_mixerCosOutput_allowed(ty);}
};

class qValueAttrib: public Tango::Attr
{
public:
	qValueAttrib():Attr("qValue", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~qValueAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TIMIQ *>(dev))->read_qValue(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TIMIQ *>(dev))->write_qValue(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TIMIQ *>(dev))->is_qValue_allowed(ty);}
};

class iValueAttrib: public Tango::Attr
{
public:
	iValueAttrib():Attr("iValue", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~iValueAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TIMIQ *>(dev))->read_iValue(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TIMIQ *>(dev))->write_iValue(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TIMIQ *>(dev))->is_iValue_allowed(ty);}
};

class boardTemperatureAttrib: public Tango::Attr
{
public:
	boardTemperatureAttrib():Attr("boardTemperature", Tango::DEV_DOUBLE, Tango::READ) {};
	~boardTemperatureAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TIMIQ *>(dev))->read_boardTemperature(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TIMIQ *>(dev))->is_boardTemperature_allowed(ty);}
};

class workingFrequencyAttrib: public Tango::Attr
{
public:
	workingFrequencyAttrib():Attr("workingFrequency", Tango::DEV_DOUBLE, Tango::READ) {};
	~workingFrequencyAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TIMIQ *>(dev))->read_workingFrequency(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TIMIQ *>(dev))->is_workingFrequency_allowed(ty);}
};

//=========================================
//	Define classes for commands
//=========================================
class ResetCmd : public Tango::Command
{
public:
	ResetCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ResetCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ResetCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<TIMIQ *>(dev))->is_Reset_allowed(any);}
};



class PLLcalibrationCmd : public Tango::Command
{
public:
	PLLcalibrationCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	PLLcalibrationCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~PLLcalibrationCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<TIMIQ *>(dev))->is_PLLcalibration_allowed(any);}
};



class AcknowledgeErrorsCmd : public Tango::Command
{
public:
	AcknowledgeErrorsCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	AcknowledgeErrorsCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~AcknowledgeErrorsCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<TIMIQ *>(dev))->is_AcknowledgeErrors_allowed(any);}
};



//
// The TIMIQClass singleton definition
//

class
#ifdef WIN32
	__declspec(dllexport)
#endif
	TIMIQClass : public Tango::DeviceClass
{
public:
//	properties member data

//	add your own data members here
//------------------------------------

public:
	Tango::DbData	cl_prop;
	Tango::DbData	cl_def_prop;
	Tango::DbData	dev_def_prop;

//	Method prototypes
	static TIMIQClass *init(const char *);
	static TIMIQClass *instance();
	~TIMIQClass();
	Tango::DbDatum	get_class_property(string &);
	Tango::DbDatum	get_default_device_property(string &);
	Tango::DbDatum	get_default_class_property(string &);
	
protected:
	TIMIQClass(string &);
	static TIMIQClass *_instance;
	void command_factory();
	void get_class_property();
	void attribute_factory(vector<Tango::Attr *> &);
	void write_class_property();
	void set_default_property();
	string get_cvstag();
	string get_cvsroot();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace TIMIQ_ns

#endif // _TIMIQCLASS_H
