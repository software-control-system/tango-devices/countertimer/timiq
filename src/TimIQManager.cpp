//=============================================================================
// TimIQManager.cpp
//=============================================================================
// abstraction.......TimIQManager for TimIQ
// class.............TimIQManager
// original author...S.Minolli - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "TimIQManager.h"

namespace TIMIQ_ns
{

// ============================================================================
//- Check TIMIQ hardware pointer:
#define CHECK_TIMIQ \
    if (!m_timiq) \
    { \
	   THROW_DEVFAILED(_CPTC("INTERNAL_ERROR"), \
			_CPTC("TIMIQ hardware pointer is null!"), \
			_CPTC("CHECK_TIMIQ()")); \
    }

// ============================================================================
// SOME USER DEFINED MESSAGES FOR THE Manager task
// ============================================================================
#define kWRITE_IVAL_MSG (yat::FIRST_USER_MSG + 1001)
#define kWRITE_QVAL_MSG (yat::FIRST_USER_MSG + 1002)
#define kSEND_CMD_MSG   (yat::FIRST_USER_MSG + 1003)
#define kREAD_IVAL_MSG  (yat::FIRST_USER_MSG + 1004)
#define kREAD_QVAL_MSG  (yat::FIRST_USER_MSG + 1005)
#define kREAD_STATE_MSG  (yat::FIRST_USER_MSG + 1006)
#define kREAD_STATUS_MSG (yat::FIRST_USER_MSG + 1007)
#define kREAD_TEMP_MSG   (yat::FIRST_USER_MSG + 1008)
#define kREAD_MXCOS_MSG  (yat::FIRST_USER_MSG + 1009)
#define kREAD_MXSIN_MSG  (yat::FIRST_USER_MSG + 1010)

// timeout for manager task (in seconds)
#define kMSG_TMO 2000

// ============================================================================

// ============================================================================
// TimIQManager::TimIQManager ()
// ============================================================================ 
TimIQManager::TimIQManager (Tango::DeviceImpl * hostDevice)
: yat4tango::DeviceTask(hostDevice)
{
  //- trace/profile this method
  yat4tango::TraceHelper t("TimIQManager::TimIQManager", this);

  this->enable_timeout_msg(false);
  this->enable_periodic_msg(false);
  this->throw_on_post_msg_timeout(true);

  m_state = Tango::INIT;
  m_status = "Init in progress...";
  m_timiq = NULL;

  m_iValue = yat::IEEE_NAN;
  m_qValue = yat::IEEE_NAN;
  m_mixerCosOutput = yat::IEEE_NAN;
  m_mixerSinOutput = yat::IEEE_NAN;
  m_boardTemperature = yat::IEEE_NAN;
}

// ============================================================================
// TimIQManager::~TimIQManager ()
// ============================================================================ 
TimIQManager::~TimIQManager ()
{
  this->enable_periodic_msg(false);

  if (m_timiq)
  {
    delete m_timiq;
    m_timiq = NULL;
  }
}

// ============================================================================
// TimIQManager::init ()
// ============================================================================ 
void TimIQManager::init(TimIQConfig& cfg)
  throw (Tango::DevFailed)
{
  m_cfg = cfg;

  //- Instanciate the TIMIQ manager object
  m_timiq = new TIMIQLib_ns::TIMIQLib();

  // check the object
  if (!m_timiq)
  {
    THROW_DEVFAILED(
      _CPTC("INTERNAL_ERROR"),
	_CPTC("TIMIQ hardware pointer is null!"),
	_CPTC("TimIQManager::init()"));
  }

  try
  {
    //- initialize the TimIQ library
    m_timiq->init(m_cfg.iPaddress, m_cfg.tcpPort);
  }
  catch (TIMIQLib_ns::Exception& Err)
  {
    Tango::DevFailed df = timiqToTangoException(Err);
    ERROR_STREAM << "TimIQManager::init() TimIQ library init caught DevFAILED: \n" << df << std::endl;
    RETHROW_DEVFAILED(df,
		  _CPTC("INTERNAL_ERROR"),
		  _CPTC("Failed to initialize hardware - Caught DevFailed!"),
		  _CPTC("TimIQManager::init()"));
  }
  catch (...)
  {	
    ERROR_STREAM << "TimIQManager::init() TimIQ library init caught [...]!" << std::endl;
    THROW_DEVFAILED(
      _CPTC("INTERNAL_ERROR"),
	_CPTC("Failed to initialize hardware - Caught [...]!"),
	_CPTC("TimIQManager::init()"));
  }

  DEBUG_STREAM << "Initialization of manager done." << std::endl;
}

// ============================================================================
// TimIQManager::get_state ()
// ============================================================================ 
Tango::DevState TimIQManager::get_state()
{
  // post message to synchronize with periodic job update
  try
  {
    this->wait_msg_handled(kREAD_STATE_MSG, kMSG_TMO);
  }
  catch(...)
  {
    m_state = Tango::FAULT;
  }

  return m_state;
}

// ============================================================================
// TimIQManager::get_status ()
// ============================================================================ 
std::string TimIQManager::get_status()
{
  // post message to synchronize with periodic job update
  try
  {
    this->wait_msg_handled(kREAD_STATUS_MSG, kMSG_TMO);
  }
  catch(...)
  {
    m_status = "Cannot get status!";
  }

  return m_status;
}

// ============================================================================
// TimIQManager::get_iValue ()
// ============================================================================ 
double TimIQManager::get_iValue()
  throw (Tango::DevFailed)
{
  // post message to synchronize with periodic job update
  try
  {
    this->wait_msg_handled(kREAD_IVAL_MSG, kMSG_TMO);
  }
  catch(...)
  {
    throw;
  }

  return m_iValue;
}

// ============================================================================
// TimIQManager::get_qValue ()
// ============================================================================ 
double TimIQManager::get_qValue()
  throw (Tango::DevFailed)
{
  // post message to synchronize with periodic job update
  try
  {
    this->wait_msg_handled(kREAD_QVAL_MSG, kMSG_TMO);
  }
  catch(...)
  {
    throw;
  }

  return m_qValue;
}

// ============================================================================
// TimIQManager::get_boardTemperature ()
// ============================================================================ 
double TimIQManager::get_boardTemperature()
  throw (Tango::DevFailed)
{
  // post message to synchronize with periodic job update
  try
  {
    this->wait_msg_handled(kREAD_TEMP_MSG, kMSG_TMO);
  }
  catch(...)
  {
    throw;
  }

  return m_boardTemperature;
}

// ============================================================================
// TimIQManager::get_mixerCosOutput ()
// ============================================================================ 
double TimIQManager::get_mixerCosOutput()
  throw (Tango::DevFailed)
{
  // post message to synchronize with periodic job update
  try
  {
    this->wait_msg_handled(kREAD_MXCOS_MSG, kMSG_TMO);
  }
  catch(...)
  {
    throw;
  }

  return m_mixerCosOutput;
}

// ============================================================================
// TimIQManager::get_mixerSinOutput ()
// ============================================================================ 
double TimIQManager::get_mixerSinOutput()
  throw (Tango::DevFailed)
{
  // post message to synchronize with periodic job update
  try
  {
    this->wait_msg_handled(kREAD_MXSIN_MSG, kMSG_TMO);
  }
  catch(...)
  {
    throw;
  }

  return m_mixerSinOutput;
}

// ============================================================================
// TimIQManager::set_iValue ()
// ============================================================================ 
void TimIQManager::set_iValue(double val)
  throw (Tango::DevFailed)
{
  // send message in queue to be sure that board has finished
  // action (time cycle is about 30ms)
  this->wait_msg_handled(kWRITE_IVAL_MSG, val, kMSG_TMO);
}

// ============================================================================
// TimIQManager::set_qValue ()
// ============================================================================ 
void TimIQManager::set_qValue(double val)
  throw (Tango::DevFailed)
{
  // send message in queue to be sure that board has finished
  // action (time cycle is about 30ms)
  this->wait_msg_handled(kWRITE_QVAL_MSG, val, kMSG_TMO);
}

// ============================================================================
// TimIQManager::set_command ()
// ============================================================================ 
void TimIQManager::set_command(TIMIQLib_ns::E_timiq_cmd_t cmd, bool have_to_wait)
  throw (Tango::DevFailed)
{
  if (have_to_wait)
  {
    //- post msg to the task
    this->wait_msg_handled(kSEND_CMD_MSG, cmd, kMSG_TMO);
  }
  else
  {
    yat::Message * msg = new yat::Message(kSEND_CMD_MSG);

    if (!msg)
    {
      ERROR_STREAM << "TimIQManager::set_command() yat::Message allocation failed!" << std::endl;
      THROW_DEVFAILED(
        _CPTC("DEVICE_ERROR"), 
        _CPTC("Failed to send message for command writing!"), 
        _CPTC("TimIQManager::set_command()")); 
    }

    // attach value
    msg->attach_data(cmd);

    //- post msg to the task
    this->post(msg);
  }    
}

// ============================================================================
// TimIQManager::process_message
// ============================================================================
void TimIQManager::process_message (yat::Message& msg)
  throw (Tango::DevFailed)
{
  //- handle msg
  switch (msg.type())
  {
    //- THREAD_INIT ----------------------
    case yat::TASK_INIT:
      {
        DEBUG_STREAM << "TimIQManager::process_message::THREAD_INIT::thread is starting up" << std::endl;
	this->init_i();
      } 
    break;

    //- THREAD_EXIT ----------------------
    case yat::TASK_EXIT:
      {
	DEBUG_STREAM << "TimIQManager::process_message::THREAD_EXIT::thread is quitting" << std::endl;
      }
    break;

    //- THREAD_PERIODIC ------------------
    case yat::TASK_PERIODIC:
      {
	//DEBUG_STREAM << "TimIQManager::process_message::THREAD_PERIODIC" << std::endl;
        //this->msgq_statistics().dump();
	this->periodic_job_i();
      }
    break;

    //- kWRITE_IVAL_MSG ------------------
    case kWRITE_IVAL_MSG:
    {
      DEBUG_STREAM << "TimIQManager::process_message::kWRITE_IVAL_MSG" << std::endl;
		
      double lVal = msg.get_data<double>();
      this->set_iValue_i(lVal);
      // update data after write
      this->periodic_job_i();
    }
    break;

    //- kWRITE_QVAL_MSG ------------------
    case kWRITE_QVAL_MSG:
    {
      DEBUG_STREAM << "TimIQManager::process_message::kWRITE_QVAL_MSG" << std::endl;
		
      double lVal = msg.get_data<double>();
      this->set_qValue_i(lVal);
      // update data after write
      this->periodic_job_i();
    }
    break;

    //- kSEND_CMD_MSG ------------------
    case kSEND_CMD_MSG:
    {
      DEBUG_STREAM << "TimIQManager::process_message::kSEND_CMD_MSG" << std::endl;
		
      TIMIQLib_ns::E_timiq_cmd_t lCmd = msg.get_data<TIMIQLib_ns::E_timiq_cmd_t>();
      this->set_command_i(lCmd);
    }
    break;

    //- kREAD_IVAL_MSG ----------------------
    case kREAD_IVAL_MSG:
      {
	//DEBUG_STREAM << "TimIQManager::process_message::kREAD_IVAL_MSG::read iValue" << std::endl;
      }
    break;

    //- kREAD_QVAL_MSG ----------------------
    case kREAD_QVAL_MSG:
    {
	//DEBUG_STREAM << "TimIQManager::process_message::kREAD_QVAL_MSG::read qValue" << std::endl;
    }
    break;

    //- kREAD_STATE_MSG ----------------------
    case kREAD_STATE_MSG:
      {
	//DEBUG_STREAM << "TimIQManager::process_message::kREAD_STATE_MSG::read state" << std::endl;
      }
    break;

    //- kREAD_STATUS_MSG ----------------------
    case kREAD_STATUS_MSG:
      {
	//DEBUG_STREAM << "TimIQManager::process_message::kREAD_STATUS_MSG::read status" << std::endl;
      }
    break;

    //- kREAD_TEMP_MSG ----------------------
    case kREAD_TEMP_MSG:
      {
	//DEBUG_STREAM << "TimIQManager::process_message::kREAD_TEMP_MSG::read temperature" << std::endl;
      }
    break;

    //- kREAD_MXCOS_MSG ----------------------
    case kREAD_MXCOS_MSG:
      {
	//DEBUG_STREAM << "TimIQManager::process_message::kREAD_MXCOS_MSG::read mixer cos" << std::endl;
      }
    break;

    //- kREAD_MXSIN_MSG ----------------------
    case kREAD_MXSIN_MSG:
      {
	//DEBUG_STREAM << "TimIQManager::process_message::kREAD_MXSIN_MSG::read mixer sin" << std::endl;
      }
    break;

    //- UNHANDLED MSG --------------------
    default:
      DEBUG_STREAM << "TimIQManager::handle_message::unhandled msg type received" << std::endl;
    break;
  }
}

// ============================================================================
// TimIQManager::init_i ()
// ============================================================================ 
void TimIQManager::init_i()
{
  //- update all for the first time - means exec. period job
  try
  {
    //- do the periodic job
    this->periodic_job_i();
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << df << std::endl;
    RETHROW_DEVFAILED(df,
      _CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to update data for 1st time"), 
	_CPTC("TimIQManager::init_i")); 
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to do periodic job" << std::endl;
    THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to update data for 1st time"), 
	_CPTC("TimIQManager::init_i")); 
  }

  //- set polling period
  size_t period = (size_t)(m_cfg.pollingPeriod);
  this->set_periodic_msg_period(period);
  this->enable_periodic_msg(true);
}

// ============================================================================
// TimIQManager::periodic_job_i ()
// ============================================================================ 
void TimIQManager::periodic_job_i()
  throw (Tango::DevFailed)
{
  // time logs
  //struct timeval begin, inter, end;
  //gettimeofday(&begin, NULL);

  // if already in FAULT, do not evaluate state
  if (m_state == Tango::FAULT)
  {
    return;
  }

  CHECK_TIMIQ;

  //- get TimIQ equipment state and status 	
  TIMIQLib_ns::E_timiq_code_t l_eqptState;
  std::string l_eqptStatus;
  Tango::DevState l_state;
  std::string l_status;

  try
  {
    l_eqptState = m_timiq->get_state(l_eqptStatus);
  }
  catch (TIMIQLib_ns::Exception& Err)
  {
    Tango::DevFailed df = timiqToTangoException(Err);
    ERROR_STREAM << "TimIQManager::periodic_job_i() caught exception: \n" << df << std::endl;
    m_state = Tango::FAULT;
    m_status = "Failed to update board state!";
    return;
  }
  catch (...)
  {	
    ERROR_STREAM << "TimIQManager::periodic_job_i() caught [...]!" << std::endl;
    m_state = Tango::FAULT;
    m_status = "Failed to update board state!";
    return;
  }

  // compose manager state & status according to HW state & status
  switch (l_eqptState)
  {
    //- System in Error
    case TIMIQLib_ns::Error:
      l_state = Tango::FAULT;
      l_status = "Board in error";
    break;
		
    //- System Up, but busy ...
    case TIMIQLib_ns::Busy:
      l_state = Tango::MOVING;
      l_status = "Board is busy";
    break;
		
    //- System Up and running
    case TIMIQLib_ns::OK:
      l_state = Tango::RUNNING;
      l_status = "Board is up and ready";
    break;
		
    //- System Up, but warming up
    case TIMIQLib_ns::Warning:
      l_state =  Tango::ALARM;
      l_status = "Board is warming up";
    break;
		
    case TIMIQLib_ns::Undefined:
    default:
      l_state = Tango::FAULT;
      l_status = "Got undefined state from board!";
    break;
  }

  // time logs
  //gettimeofday(&inter, NULL);

  // update board data (if not MOVING nor FAULT)
  if ((l_state != Tango::FAULT) && 
      (l_state != Tango::MOVING))
  {
    TIMIQLib_ns::timIQval_t val;

    try
    {
      m_timiq->get_boardData(val);
      m_iValue = (double)val.iVal;
      m_qValue = (double)val.qVal;
      m_mixerCosOutput = (double)val.mixerCos;
      m_mixerSinOutput = (double)val.mixerSin;
      m_boardTemperature = (double)val.temperature;
    }
    catch (TIMIQLib_ns::Exception& Err)
    {
      Tango::DevFailed df = timiqToTangoException(Err);
      ERROR_STREAM << df << std::endl;
      m_state = Tango::FAULT;
      m_status = "Failed to update board values!";
      return;
    }
    catch (...)
    {
      ERROR_STREAM << "Failed to update board values - caught [...]!" << std::endl;
      m_state = Tango::FAULT;
      m_status = "Failed to update board values!";
      return;
    }

    //- Check board temperature to set ALARM state (if necessary)
    if ((m_boardTemperature < m_cfg.minTemp) || 
        (m_boardTemperature > m_cfg.maxTemp))
    {
      WARN_STREAM << "Board temperature out of bounds: " << m_boardTemperature 
        << ", tmin = " << m_cfg.minTemp << " - tmax = " << m_cfg.maxTemp << std::endl;
	    std::string l_msg = m_status;
      l_status = "Board temperature out of bounds!";
      l_state = Tango::ALARM;
    }
  }

  // set internal state & status with evaluated values
  m_state = l_state;
  m_status = l_status;

  // time logs
  /*
  gettimeofday(&end, NULL);
  DEBUG_STREAM << "INSTRU - periodic_job_i() function took " 
               << (end.tv_sec * 1000.0 + end.tv_usec / 1000.0) - (begin.tv_sec * 1000.0 + begin.tv_usec / 1000.0) 
               << " ms - getstate took: " 
               << (inter.tv_sec * 1000.0 + inter.tv_usec / 1000.0) - (begin.tv_sec * 1000.0 + begin.tv_usec / 1000.0) 
               << std::endl;
  */
}

// ======================================================================
// TimIQManager::set_iValue_i
// ======================================================================
void TimIQManager::set_iValue_i(double val)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "TimIQManager::set_iValue_i() write value = " << val << std::endl;

  CHECK_TIMIQ;

  try
  {
    float l_iValue = (float)val;
    m_timiq->set_iValue(l_iValue);			
  }
  catch (TIMIQLib_ns::Exception& Err)
  {
    Tango::DevFailed df = timiqToTangoException(Err);
    ERROR_STREAM << "TimIQManager::set_iValue_i() caught exception: \n" << df << std::endl;
    RETHROW_DEVFAILED(df,
		  _CPTC("DEVICE_ERROR"),
		  _CPTC("Failed to set I value!"),
		  _CPTC("TimIQManager::set_iValue_i()"));
  }
  catch (...)
  {
    ERROR_STREAM << "TimIQManager::set_iValue_i() caught [...]!" << std::endl;
    THROW_DEVFAILED(
		_CPTC("DEVICE_ERROR"),			
		_CPTC("Failed to set I value!"),
		_CPTC("TimIQManager::set_iValue_i()"));
  }
}

// ======================================================================
// TimIQManager::set_qValue_i
// ======================================================================
void TimIQManager::set_qValue_i(double val)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "TimIQManager::set_qValue_i() write value = " << val << std::endl;

  CHECK_TIMIQ;

  try
  {
    float l_qValue = (float)val;
    m_timiq->set_qValue(l_qValue);			
  }
  catch (TIMIQLib_ns::Exception& Err)
  {
    Tango::DevFailed df = timiqToTangoException(Err);
    ERROR_STREAM << "TimIQManager::set_qValue_i() caught exception: \n" << df << std::endl;
    RETHROW_DEVFAILED(df,
		  _CPTC("DEVICE_ERROR"),
		  _CPTC("Failed to set Q value!"),
		  _CPTC("TimIQManager::set_qValue_i()"));
  }
  catch (...)
  {
    ERROR_STREAM << "TimIQManager::set_qValue_i() caught [...]!" << std::endl;
    THROW_DEVFAILED(
		_CPTC("DEVICE_ERROR"),			
		_CPTC("Failed to set Q value!"),
		_CPTC("TimIQManager::set_qValue_i()"));
  }
}

// ======================================================================
// TimIQManager::set_command_i
// ======================================================================
void TimIQManager::set_command_i(TIMIQLib_ns::E_timiq_cmd_t cmd)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "TimIQManager::set_command_i() write command = " << cmd << std::endl;

  CHECK_TIMIQ;

  try
  {		
    m_timiq->set_command(cmd);
  }
  catch (TIMIQLib_ns::Exception& Err)
  {
    Tango::DevFailed df = timiqToTangoException(Err);
    ERROR_STREAM << "TimIQManager::set_command_i() caught exception: \n" << df << std::endl;
    RETHROW_DEVFAILED(df,
		  _CPTC("DEVICE_ERROR"),
		  _CPTC("Failed to send command!"),
		  _CPTC("TimIQManager::set_command_i()"));
  }
  catch (...)
  {
    ERROR_STREAM << "TimIQManager::set_command_i() caught [...]!" << std::endl;
    THROW_DEVFAILED(
		_CPTC("DEVICE_ERROR"),			
		_CPTC("Failed to send command!"),
		_CPTC("TimIQManager::set_command_i()"));
  }
}

// ======================================================================
// TimIQManager::timiqToTangoException
// ======================================================================
Tango::DevFailed TimIQManager::timiqToTangoException (const TIMIQLib_ns::Exception& te)
{
  Tango::DevErrorList error_list(te.errors.size());
  error_list.length(te.errors.size());

  for (unsigned int i = 0; i < te.errors.size(); i++)
  {
	  error_list[i].reason = CORBA::string_dup(te.errors[i].reason.c_str());
	  //ERROR_STREAM << "timiqToTangoException -> REASON = " << error_list[i].reason << std::endl;
	  error_list[i].desc = CORBA::string_dup(te.errors[i].desc.c_str());
	  //ERROR_STREAM << "timiqToTangoException -> DESC = " << error_list[i].desc << std::endl;
	  error_list[i].origin = CORBA::string_dup(te.errors[i].origin.c_str());
	  //ERROR_STREAM << "timiqToTangoException -> ORIGIN = " << error_list[i].origin << std::endl;

	  switch(te.errors[i].severity)
	  {
	  case TIMIQLib_ns::WARN:
		  error_list[i].severity = Tango::WARN;
		  break;
	  case TIMIQLib_ns::PANIC:
		  error_list[i].severity = Tango::PANIC;
		  break;
	  case TIMIQLib_ns::ERR:
	  default:
		  error_list[i].severity = Tango::ERR;
		  break;
	  }
  }

  return Tango::DevFailed(error_list);
}

} // namespace TIMIQ_ns

