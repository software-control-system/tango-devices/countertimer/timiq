from conan import ConanFile

class TimIQRecipe(ConanFile):
    name = "timiq"
    executable = "ds_TimIQ"
    version = "1.0.1"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Jacques Gouno, Alain Buteau"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/countertimer/timiq.git"
    description = "TimIQ device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("timiqlib/[>=1.0]@soleil/stable")
        self.requires("curl/7.37.0@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
